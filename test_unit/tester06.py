#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Saturday Feb 02

@author: andrei
"""
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import selenium.common.exceptions
import datetime
import time
import unittest
from cfg import param


class Bank99Tests(unittest.TestCase):

    def setUp(self):
        """Set up for test"""
        USER_AGENT = "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:52.0) " + \
            "Gecko/20100101 Firefox/52.0"
        profile = webdriver.FirefoxProfile()
        profile.set_preference("general.useragent.override", USER_AGENT)
        self.driver = webdriver.Firefox(profile)

    def test_login_ok(self):
        NAME1 = "uid"
        NAME2 = "password"
        NAME3 = "btnLogin"

        login = param["login"]
        password = param["password"]
        PAGE1 = param["base_url"]

        try:
            driver = self.driver
            driver.get(PAGE1)

            WebDriverWait(driver, 5).until(
                EC.title_is("Guru99 Bank Home Page"))

            login_fld = driver.find_element_by_name(NAME1)
            password_fld = driver.find_element_by_name(NAME2)
            login_fld.send_keys(login)
            password_fld.send_keys(password)

            button = driver.find_element_by_name(NAME3)
            button.click()
        except:
            print("Exception, test failed")
            self.assertTrue(False)
            return

        try:
            WebDriverWait(driver, 5).until(
                EC.title_is("Guru99 Bank Manager HomePage"))
            el = driver.find_element_by_xpath(
                "//table/tbody/tr[@class='heading3']/td")
            self.assertEqual(el.text, "Manger Id : %s" % (login))

            FILENAME = "./login_ok_%s.png" % (
                datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))

            driver.get_screenshot_as_file(FILENAME)            
            return

        except selenium.common.exceptions.TimeoutException:
            print("Timeout Exception, test failed")
            self.assertTrue(False)

    def test_login_invalid(self):
        NAME1 = "uid"
        NAME2 = "password"
        NAME3 = "btnLogin"

        LOGIN = "wronglogin"
        PASSWORD = "wrongpassword"
        PAGE1 = param["base_url"]
        popup_text = 'User or Password is not valid'

        try:
            driver = self.driver
            driver.get(PAGE1)

            WebDriverWait(driver, 10).until(
                EC.title_is("Guru99 Bank Home Page"))

            login_fld = driver.find_element_by_name(NAME1)
            password_fld = driver.find_element_by_name(NAME2)
            login_fld.send_keys(LOGIN)
            password_fld.send_keys(PASSWORD)

            button = driver.find_element_by_name(NAME3)
            button.click()

        except Exception as e:
            print("Exception, test failed")
            self.assertTrue(False)
            return

        try:
            WebDriverWait(driver, 10).until(EC.alert_is_present(), popup_text)
            alert = driver.switch_to.alert
            alert.accept()
            print("alert accepted, test successfull")
            self.assertTrue(True)

        except selenium.common.exceptions.TimeoutException:
            print("no alert, test failed")
            self.assertTrue(False)

    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()
