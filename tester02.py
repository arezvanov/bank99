#!/usr/bin/python3
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import selenium.common.exceptions
from cfg import param


def create_driver_ff():
    profile = webdriver.FirefoxProfile()
    profile.set_preference("general.useragent.override",
                           "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0")
    return webdriver.Firefox(profile)


def create_driver_pjs():
    dcap = dict(DesiredCapabilities.PHANTOMJS)
    dcap["phantomjs.page.settings.userAgent"] = (
        "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0 "
    )
    return webdriver.PhantomJS(desired_capabilities=dcap)


def create_driver_chrome():
    return webdriver.Chrome()


def main():
    NAME1 = "uid"
    NAME2 = "password"
    NAME3 = "btnLogin"

    LOGIN = param["login"]
    PASSWORD = param["password"]
    PAGE1 = param["base_url"]

    try:
        driver = create_driver_chrome()
    except Exception as e:
        print(e)
        return

    try:
        driver.get(PAGE1)

        WebDriverWait(driver, 10).until(EC.title_is("Guru99 Bank Home Page"))

        login_fld = driver.find_element_by_name(NAME1)
        password_fld = driver.find_element_by_name(NAME2)
        login_fld.send_keys(LOGIN)
        password_fld.send_keys(PASSWORD)

        button = driver.find_element_by_name(NAME3)
        button.click()

    except Exception as e:
        print("Exception, test failed")
        driver.quit()
        return

    try:
        WebDriverWait(driver, 10).until(
            EC.title_is("Guru99 Bank Manager HomePage"))
        print("Test successfull")
        driver.quit()
        return

    except selenium.common.exceptions.TimeoutException:
        print("Timeout Exception, test failed")
        driver.quit()
        return

if __name__ == "__main__":
    main()
