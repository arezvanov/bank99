#!/usr/bin/python3
from pyquery import PyQuery
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
import selenium.common.exceptions
import datetime
import time


def main():
    PAGE1 = "http://www.demo.guru99.com/V4/index.php"

    NAME1 = "uid"
    NAME2 = "password"
    NAME3 = "btnLogin"

    login = "mngr175960"
    password = "emabeqE"

    try:
        driver = webdriver.Firefox()
        driver.get(PAGE1)

        WebDriverWait(driver, 10).until(EC.title_is("Guru99 Bank Home Page"))

        login_fld = driver.find_element_by_name(NAME1)
        password_fld = driver.find_element_by_name(NAME2)
        login_fld.send_keys(login)
        password_fld.send_keys(password)

        button = driver.find_element_by_name(NAME3)
        button.click()
    except:
        print("Exception, test failed")
        return

    try:
        WebDriverWait(driver, 10).until(
            EC.title_is("Guru99 Bank Manager HomePage"))
        print("Test successfull")
        return

    except selenium.common.exceptions.TimeoutException:
        print("Timeout Exception, test failed")
        return

    driver.quit()

if __name__ == "__main__":
    main()
