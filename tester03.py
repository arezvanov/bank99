#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thursday Jan 31

@author: andrei
"""

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import selenium.common.exceptions
#from cfg03 import param
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile


def create_driver_ff():
    profile = webdriver.FirefoxProfile()
    profile.set_preference("general.useragent.override",
                           "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0")
    return webdriver.Firefox(profile)


def create_driver_pjs():
    dcap = dict(DesiredCapabilities.PHANTOMJS)
    dcap["phantomjs.page.settings.userAgent"] = (
        "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0 "
    )
    return webdriver.PhantomJS(desired_capabilities=dcap)


def create_driver_chrome():
    return webdriver.Chrome()


def checker(login, password, base_url, popup_text):
    NAME1 = "uid"
    NAME2 = "password"
    NAME3 = "btnLogin"

    LOGIN = login
    PASSWORD = password
    PAGE1 = base_url

    try:
        driver = create_driver_ff()
    except Exception as e:
        print(e)
        return

    try:
        driver.get(PAGE1)

        WebDriverWait(driver, 10).until(EC.title_is("Guru99 Bank Home Page"))

        login_fld = driver.find_element_by_name(NAME1)
        password_fld = driver.find_element_by_name(NAME2)
        login_fld.send_keys(LOGIN)
        password_fld.send_keys(PASSWORD)

        button = driver.find_element_by_name(NAME3)
        button.click()

    except Exception as e:
        print("Exception, test failed")
        driver.quit()
        return

    try:
        WebDriverWait(driver, 10).until(EC.alert_is_present(), popup_text)
        alert = driver.switch_to.alert
        alert.accept()
        print("alert accepted, test successfull")

    except selenium.common.exceptions.TimeoutException:
        print("no alert, test failed")

    finally:
        driver.quit()


def main():
    param = pd.read_excel('test03.xlsx', sheet_name='sheet1')
    for i in param.index:
        checker(param["login"][i], param["password"][i], param[
                "base_url"][i], 'User or Password is not valid')

if __name__ == "__main__":
    main()
